<?php 
include '../temp/sidebar-menu.php';
 include '../temp/header.php';
include '../config/koneksi.php';

if (isset($_GET['id'])) {
  $id_job = $_GET['id'];
}else{
  die("error no id selected");
}


$query = "SELECT * FROM job_vacancy WHERE id_job='$id_job'";
      $sql = mysqli_query($conn, $query);
      $hasil = mysqli_fetch_array($sql);
      $id_job = $hasil['id_job'];
      $posisi_job = $hasil['posisi_job'];
      $wilayah = $hasil['wilayah'];
      $deskripsi = $hasil['deskripsi'];
      $deskripsi_2 = $hasil['deskripsi_2'];
      $tanggal_posting = $hasil['tanggal_posting'];
      $end_date = $hasil['end_date'];

if(isset($_POST['edit'])){
    $posisi_job = $_POST['posisi_job'];
    $wilayah = $_POST['wilayah'];
    $deskripsi = $_POST['deskripsi'];
    $deskripsi_2 = $_POST['deskripsi_2'];
    $tanggal_posting = $_POST['tanggal_posting'];
    $end_date = $_POST['end_date'];
    $status = $_POST['status'];

     $sql = "UPDATE job_vacancy SET posisi_job = '$posisi_job', wilayah = '$wilayah', deskripsi = '$deskripsi', 
            deskripsi_2 = '$deskripsi_2', tanggal_posting = '$tanggal_posting',end_date = '$end_date', status = '$status' WHERE id_job = '$id_job' ";
        $update = mysqli_query($conn, $sql);

        if ($update) {
          echo "<script>alert('Data telah Diupdate') 
          window.location.href = 'job.php'
          </script>";
        } else {
          echo "<script>alert('Terdapat Kesalahan') 
          window.location.href = 'edit_job.php'
          </script>";
        }

}


 ?>

<div class="header-body">
  	<div class="row align-items-center py-4">
    <div class="col-lg-6 col-7">
      <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
          <li class="breadcrumb-item"><a href="#">JOB Vacancy</a></li>
          <li class="breadcrumb-item active" aria-current="page">Edit Job</li>
        </ol>
      </nav>
    </div>
	</div>
</div>


 <div class="container-fluid card-primary">
              <div class="card-header">
                <h3 class="card-title">JOB Vacancy</h3>
              <form role="form" method="post">
                  <input type="hidden" name="id_job" value="<?= $hasil['id_job']; ?>">

                <div class="card-body">


              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Posisi Job</label>
                    <input type="text" class="form-control" name="posisi_job" value="<?php echo $posisi_job ?>" id="exampleInputEmail1" placeholder="Enter Posisi">
                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">wilayah</label>
                    <select id = "wilayah" name="wilayah" class="form-control">
                   <?php
                    $proList = array(
                        "Jakarta Utara" => "Jakarta Utara",
                          "Jakarta Selatan" => "Jakarta Selatan",
                          "Jakarta Pusat" => "Jakarta Pusat",
                          "Cikampek" => "Cikampek",
                          "Cibitung" => "Cibitung",
                          "Karawang" => "Karawang",
                      );
                    
                    
                    asort($proList);
                    reset($proList); 
                    foreach($proList as $r => $s):
                    ?>
                    
                    <option value="<?php echo $r; ?>" <?php if($hasil['wilayah']==$s) echo 'selected="selected"'; ?>>
                    <?php echo $s;
                        endforeach;
                    ?>
                  </option>
                    </select>

                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">deskripsi</label>
                    <input type="text" class="form-control" name="deskripsi" id="exampleInputEmail1" value="<?php echo $deskripsi ?>" placeholder="Enter deskripsi">
                  </div>
                 </div>

                 <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">deskripsi recruitment</label>
                      <textarea type="text" class="form-control" name="deskripsi_2" id="exampleInputEmail1" placeholder="Enter deskripsi recruitment"><?php echo $deskripsi_2 ?></textarea>
                    </div>
                  </div>
                </div>
          


                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputFile">Start Date</label>
                      <div class="input-group">
  					           	<input type="date" id="tanggal" class="form-control" name="tanggal_posting" value="<?php echo $tanggal_posting?>">

                      </div>
                    </div>
                   </div>

                   <div class="col-md-6">
                      <div class="form-group">
                      <label for="exampleInputFile">End Date</label>
                      <div class="input-group">
                        <input type="date" id="tanggal" class="form-control" name="end_date" value="<?php echo $end_date?>">

                      </div>
                    </div>
                  </div>
                </div>

                  <div class="form-group">
                    <label for="exampleInputFile">Status</label>
                  <select id = "status" name="status" class="form-control">
                   <?php
                    $proList = array(
                        "ACTIVE" => "ACTIVE",
                        "INACTIVE" => "INACTIVE",
                        "NEW" => "NEW",
                      );
                    
                    
                    asort($proList);
                    reset($proList); 
                    foreach($proList as $r => $s):
                    ?>
                    
                    <option value="<?php echo $r; ?>" <?php if($hasil['status']==$s) echo 'selected="selected"'; ?>>
                <?php echo $s;
                    endforeach;
                    ?>
                    </select>

                  </div>

                     <button type="submit" class="btn btn-primary col-md-12" name="edit">Update</button>
                </div>
                <!-- /.card-body -->
                 
                </div>
              </form>
          </div>

          <?php
 include '../temp/footer.php';

 ?>