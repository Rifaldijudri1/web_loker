<?php 

include '../config/koneksi.php';

session_start();

 if(empty($_SESSION['id_login'])){

     header('location:login.php');

 }else{

 }

include '../temp/sidebar-menu.php';

 include '../temp/header.php';

 ?>

<style type="text/css">
  td { white-space:pre }
</style>

 <div class="header bg-primary pb-6">

      <div class="container-fluid">

        <div class="header-body">

          <div class="row align-items-center py-4">

            <div class="col-lg-6 col-7">

              <h6 class="h2 text-dark d-inline-block mb-0">Job Vacancy</h6>

              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">

                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">

                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>

                  <li class="breadcrumb-item"><a href="#">Job Vacancy</a></li>

                  <li class="breadcrumb-item active" aria-current="page">Job Vacancy</li>

                </ol>

              </nav>

            </div>

           <!--  <div class="col-lg-6 col-5 text-right">

              <a href="#" class="btn btn-sm btn-neutral">New</a>

              <a href="#" class="btn btn-sm btn-neutral">Filters</a>

            </div> -->

          </div>

        </div>

      </div>

    </div>

    <!-- Page content -->

    <div class="container-fluid mt--6">

      <div class="row">

        <div class="col">

          <div class="card">

            <!-- Card header -->

            <div class="card-header border-0">

              <?php 

              if ($_SESSION['level'] == 'super_admin') {

                

                //<a href="tambah_job.php" class="btn btn-sm btn-primary">Tambah Job</a>

                echo "<a href=\"tambah_job.php\" class=\"btn btn-outline-primary btn-sm\">Tambah Job</a>";

              }

               ?>

  

            </div>

            <!-- Light table -->

            <div class="table-responsive py-4">

    <table class="table table-responsive">

        <thead class="thead-light">

            <tr>

                 	    <th style="width: 16.66%">No </th>

                      <th style="width: 16.66%">Position</th>

                      <th>Location</th>

                      <th>Job Description</th>

                      <th class="col-md-2">Kualifikasi</th>

                      <th>Date Postingan</th>

                      <th>End Postingan</th>

                      <th>Status</th>

                      <th>#</th>

            </tr>

        </thead>

       



        <tbody>



        	         <?php 

                    $no=1;

                    $sql = mysqli_query($conn,"SELECT * FROM job_vacancy");

                    while ($hasil = mysqli_fetch_array($sql)) {

                      // $id_tamu     = $hasil['id_tamu'];

                   ?>

            		<tr>

                	  <td >

                        <?= $no++ ?>

                      </td>

                      <td >

                        <?= $hasil['posisi_job'] ?>

                      </td>

                      <td  >

                        <?= $hasil['wilayah'] ?>

                      </td>

                      <td >

                        <?= $hasil['deskripsi'] ?>

                      </td>

                      <td >

                        <div style="width:3   0%; font-size:12px;"><?= $hasil['deskripsi_2'] ?></div>

                      </td>

                      <td  >

                        <?= $hasil['tanggal_posting'] ?>

                      </td>

                      <td  >

                        <?= $hasil['end_date'] ?>

                      </td>

                      <td  >

                        <?= $hasil['status'] ?>

                      </td>



                       <td >

                        <?php 

              if ($_SESSION['level'] == 'super_admin') {

                        echo "<a href=\"edit_job.php?id=$hasil[id_job]\" class=\"badge badge-success\">EDIT</a><br><br>";

                        echo "<a href=\"delete_job.php?id=$hasil[id_job]\" class=\"badge badge-danger\">DELETE</a>"; 

                        }

               ?>

                      </td>

            		</tr>

      		     <?php } ?>

        </tbody>

                     

    </table>

</div>

            <!-- Card footer -->

           

          </div>

        </div>

      </div>



      

      <!-- Dark table -->

    





<br><br><br><br><br><br>

 <?php

 include '../temp/footer.php';



 ?>