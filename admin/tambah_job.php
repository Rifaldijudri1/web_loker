<?php 
include '../temp/sidebar-menu.php';
 include '../temp/header.php';
include '../config/koneksi.php';

if (isset($_POST['tambah'])) {
    $posisi_job = $_POST['posisi_job'];
    $wilayah = $_POST['wilayah'];
    $deskripsi = $_POST['deskripsi'];
    $deskripsi_2 = $_POST['deskripsi_2'];
    $tanggal_posting = $_POST['tanggal_posting'];
    $end_date = $_POST['end_date'];
    $status = $_POST['status'];

    $sql = "INSERT INTO job_vacancy(posisi_job, wilayah, deskripsi, deskripsi_2, tanggal_posting,end_date, status) VALUES('$posisi_job','$wilayah','$deskripsi','$deskripsi_2','$tanggal_posting','$end_date','$status')";
    if (mysqli_query($conn,$sql)) {
        echo "<script>alert('Data berhasil di tambah')
      window.location.href='job.php'</script>";
    }else {
        header("Location:tambah_job.php");
    }
}

 ?>

<div class="header-body">
  	<div class="row align-items-center py-4">
    <div class="col-lg-6 col-7">
      <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
          <li class="breadcrumb-item"><a href="#">JOB Vacancy</a></li>
          <li class="breadcrumb-item active" aria-current="page">Tambah Job</li>
        </ol>
      </nav>
    </div>
	</div>
</div>


 <div class="container-fluid card-primary">
              <div class="card-header">
                <h3 class="card-title">JOB Vacancy</h3>
              <form role="form" method="post">
                <div class="card-body">

                  <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Posisi Job</label>
                      <input type="text" class="form-control" name="posisi_job" id="exampleInputEmail1" placeholder="Enter Posisi">
                    </div>
                  </div>

                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputFile">Wilayah</label>
                    <select name="wilayah" id="wilayah" class="form-control">
                      <option value=""></option>
                      <?php
                      $proList = array(
                          "Jakarta Utara" => "Jakarta Utara",
                          "Jakarta Selatan" => "Jakarta Selatan",
                          "Jakarta Pusat" => "Jakarta Pusat",
                          "Cikampek" => "Cikampek",
                          "Cibitung" => "Cibitung",
                          "Karawang" => "Karawang",
                        );
                      
                      
                      asort($proList);
                      reset($proList); 
                      foreach($proList as $r => $s):
                        echo '<option value="'.$s.'">'.$s.'</option>'; //close your tags!!
                      endforeach;
                      ?>
                    </select>
                  </div>
                </div>
              </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">deskripsi</label>
                      <input type="text" class="form-control" name="deskripsi" id="exampleInputEmail1" placeholder="Enter deskripsi">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">deskripsi recruitment</label>
                      <textarea type="text" class="form-control" name="deskripsi_2" id="exampleInputEmail1" placeholder="Enter deskripsi recruitment"></textarea>
                    </div>
                  </div>
                </div>

                <div class="row">
                     <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputFile">Start Date</label>
                          <div class="input-group">
      						          <input type="date" id="tanggal" name="tanggal_posting" class="form-control">
                          </div>
                      </div>
                     </div>


             
                     <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputFile">End Date</label>
                        <div class="input-group">
                            <input type="date" id="tanggal" name="end_date" class="form-control">
                        </div>
                       </div>
                  </div>
                </div>

                  <div class="form-group">
                    <label for="exampleInputFile">Status</label>
					        	<select class="form-control" name="status">
  				          	<option value="ACTIVE">ACTIVE</option>
  				          	<option value="INACTIVE">INACTIVE</option>
  				          	<option value="NEW">NEW</option>
  				          </select>
                  </div>

                  <button type="submit" class="btn btn-outline-primary col-md-12 " name="tambah">Tambah</button>

                </div>
                <!-- /.card-body -->
                  
                </div>
              </form>
          </div>

          <?php
 include '../temp/footer.php';

 ?>