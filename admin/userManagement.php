<?php 
include '../config/koneksi.php';
session_start();
 if(empty($_SESSION['id_login'])){
     header('location:login.php');
 }else{
 }
include '../temp/sidebar-menu.php';
 include '../temp/header.php';
 ?>

 <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">User Management</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">User Management</a></li>
                  <li class="breadcrumb-item active" aria-current="page">User Management</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">New</a>
              <a href="#" class="btn btn-sm btn-neutral">Filters</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
  <a href="tambah_user.php" class="btn btn-sm btn-primary">Tambag User</a>
            </div>
            <!-- Light table -->
            <div class="table-responsive py-4">
    <table class="table table-flush" id="datatable-basic1">
        <thead class="thead-light">
            <tr>
                 	  <th>No </th>
                      <th>Nama Lengkap </th>
                      <th>Username</th>
                      <th>Level</th>
            </tr>
        </thead>
       

        <tbody>

        	         <?php 
                    $no=1;
                    $sql = mysqli_query($conn,"SELECT * FROM login");
                    while ($hasil = mysqli_fetch_array($sql)) {
                      // $id_tamu     = $hasil['id_tamu'];
                   ?>
            		<tr>
                	  <td>
                        <?= $no++ ?>
                      </td>
                      <td >
                        <?= $hasil['nama_lengkap'] ?>
                      </td>
                      <td >
                        <?= $hasil['user'] ?>
                      </td>
                      <td >
                        <?= $hasil['level'] ?>
                      </td>
                     

                      
            		</tr>
      		     <?php } ?>
        </tbody>
                     
    </table>
</div>
            <!-- Card footer -->
           
          </div>
        </div>
      </div>

      
      <!-- Dark table -->
    


<br><br><br><br><br><br>
 <?php
 include '../temp/footer.php';

 ?>