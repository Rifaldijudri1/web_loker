    
<?php
  include 'header.php';
 //  session_start();
 // if(empty($_SESSION['id_login'])){
 //     header('location:login.php');
 // }else{
 //}
 ?>
<?php 
  include 'sidebar.php';
 ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
    <div class="container-fluid">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><b>DAFTAR TAMU</b></h3> 
                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>NAMA TAMU</th>
                      <th>NOMOR LOKER</th>
                      <th>TANGGAL/JAM</th>
                      <th>Action</th>
                      <th><a href="tambah_tamu.php" type="button" class="btn btn-sm btn-primary float-right">
                        Tambah Tamu
                          </a>
                      </th>
                    </tr>
                  </thead>

                  <?php 
                    $no=1;
                    $sql = mysqli_query($conn,"SELECT id_tamu,nama_tamu,no_loker,tanggal FROM tb_tamu,tb_loker
                      WHERE tb_loker.id_loker = tb_tamu.id_loker");
                    while ($hasil = mysqli_fetch_array($sql)) {
                      $id_tamu     = $hasil['id_tamu'];
                   ?>

                  <tbody>
                    <tr>
                      <td><?= $no++ ?></td>
                      <td><?= $hasil['nama_tamu'] ?></td>
                      <td><?= $hasil['no_loker'] ?></td>
                      <td><?= $hasil['tanggal'] ?></td>
                      <td><?php echo "<a href=\"edit_data_tamu.php?id=$hasil[id_tamu]\" class=\"btn btn-sm btn-success\">Edit</a>" ?>
                        <?php echo "<a href=\"hapus_data_tamu.php?id=$hasil[id_tamu]\" class=\"btn btn-sm btn-danger\">Hapus</a>"; ?></td>
                    </tr>
                  </tbody>
                <?php } ?>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
              
              <!-- /.card-header -->
              <div class="card-body pt-0">
                <!--The calendar -->
                <div id="calendar" style="width: 100%"></div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php 
  include 'footer.php';
   ?>