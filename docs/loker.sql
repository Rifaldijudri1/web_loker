-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 26 Feb 2021 pada 17.11
-- Versi server: 10.1.31-MariaDB
-- Versi PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loker`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `job_vacancy`
--

CREATE TABLE `job_vacancy` (
  `id_job` int(11) NOT NULL,
  `posisi_job` varchar(100) NOT NULL,
  `wilayah` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `deskripsi_2` text NOT NULL,
  `tanggal_posting` datetime NOT NULL,
  `status` enum('NEW','ACTIVE','INACTIVE') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `job_vacancy`
--

INSERT INTO `job_vacancy` (`id_job`, `posisi_job`, `wilayah`, `deskripsi`, `deskripsi_2`, `tanggal_posting`, `status`) VALUES
(1, 'salesygwahjagf', 'bogor', 'magang', 'SMK', '2021-02-26 20:56:00', 'ACTIVE'),
(2, 'barista', 'papua', 'Cofee', 'Experr 3 yearshhh', '2021-02-26 21:41:00', 'ACTIVE'),
(6, 'sales_1', 'bogor', 'magang', 'SMK', '2021-02-25 15:59:00', 'NEW'),
(7, 'sales_2', 'bogor', 'magang', 'SMK', '2021-02-25 15:59:00', 'NEW'),
(8, 'sales_3', 'bogor', 'magang', 'SMK', '2021-02-25 15:59:00', 'NEW'),
(9, 'sales_4', 'bogor', 'magang', 'SMK', '2021-02-25 15:59:00', 'NEW'),
(10, 'sales_5', 'bogor', 'magang', 'SMK', '2021-02-25 15:59:00', 'NEW'),
(11, 'sales_6', 'bogor', 'magang', 'SMK', '2021-02-25 15:59:00', 'NEW'),
(12, 'sales_7', 'bogor', 'magang', 'SMK', '2021-02-25 15:59:00', 'NEW'),
(13, 'sales_8', 'bogor', 'magang', 'SMK', '2021-02-25 15:59:00', 'NEW');

-- --------------------------------------------------------

--
-- Struktur dari tabel `list_appliation`
--

CREATE TABLE `list_appliation` (
  `id_kandidat` int(11) NOT NULL,
  `id_job` int(11) NOT NULL,
  `nama_kandidat` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_hp` int(11) NOT NULL,
  `tanggal_apply` date NOT NULL,
  `upload_cv` varchar(100) NOT NULL,
  `upload_foto` varchar(100) NOT NULL,
  `status` enum('new','approve','reject') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `list_appliation`
--

INSERT INTO `list_appliation` (`id_kandidat`, `id_job`, `nama_kandidat`, `email`, `no_hp`, `tanggal_apply`, `upload_cv`, `upload_foto`, `status`) VALUES
(1, 2, 'ava', 'sfsf', 342, '2021-02-26', 'dvd', 'dgdgw', 'approve'),
(2, 2, 'kna', 'afmnhwf', 253463, '2021-02-26', 'jkbfgjhs', 'usjfjhsf', 'approve'),
(6, 0, '', '', 0, '0000-00-00', 'akun.png', '', 'new'),
(7, 0, 'Cobbaaa', 'rnldredji@gmail.com', 0, '2021-02-26', '12222.png', '', 'new');

-- --------------------------------------------------------

--
-- Struktur dari tabel `login`
--

CREATE TABLE `login` (
  `id_login` int(11) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `user` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` enum('1','2','3') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `login`
--

INSERT INTO `login` (`id_login`, `nama_lengkap`, `user`, `password`, `level`) VALUES
(1, 'riko', 'riko', '206e8e5e74a2a33379e0e2be7f2ce6d1', ''),
(2, 'gatot', 'gatot', 'bb474dec2b2526c82e22c987722bbd7e', '3'),
(3, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '1');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `job_vacancy`
--
ALTER TABLE `job_vacancy`
  ADD PRIMARY KEY (`id_job`);

--
-- Indeks untuk tabel `list_appliation`
--
ALTER TABLE `list_appliation`
  ADD PRIMARY KEY (`id_kandidat`);

--
-- Indeks untuk tabel `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id_login`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `job_vacancy`
--
ALTER TABLE `job_vacancy`
  MODIFY `id_job` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `list_appliation`
--
ALTER TABLE `list_appliation`
  MODIFY `id_kandidat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `login`
--
ALTER TABLE `login`
  MODIFY `id_login` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
