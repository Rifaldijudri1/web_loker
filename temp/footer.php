  <footer class="footer pt-0">

        <div class="row align-items-center justify-content-lg-between">

          <div class="col-lg-6">

            <div class="copyright text-center  text-lg-left  text-muted">

              &copy; <?= date('Y') ?> <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">VIL-GROUP</a>

            </div>

          </div>

          <div class="col-lg-6">

           

          </div>

        </div>

      </footer>

    </div>

  </div>

  <!-- Argon Scripts -->

  <!-- Core -->

  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>

  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>

  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>

  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>

  <!-- Optional JS -->

  <script src="../assets/vendor/chart.js/dist/Chart.min.js"></script>

  <script src="../assets/vendor/chart.js/dist/Chart.extension.js"></script>

  <!-- Argon JS -->

  <script src="../assets/js/argon.js?v=1.2.0"></script>



  <script src="../assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="../assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<script src="../assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>

<script src="../assets/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>

<script src="../assets/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>

<script src="../assets/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>

<script src="../assets/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>

<script src="../assets/vendor/datatables.net-select/js/dataTables.select.min.js"></script>



<script type="text/javascript">

  

  $(document).ready(function() {

    $('#datatable-basic').DataTable();

} );



   $(document).ready(function() {

    $('#datatable-basic1').DataTable();

} );



</script>







</body>



</html>

