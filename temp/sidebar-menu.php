<html>



<head>

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">

  <meta name="author" content="Creative Tim">

  <title>LOKER</title>

  <!-- Favicon -->

  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">

  <!-- Fonts -->

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">

  <!-- Icons -->

  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">

  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">

  <!-- Page plugins -->

  <!-- Argon CSS -->

  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">



  <link rel="stylesheet" href="../assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">

<link rel="stylesheet" href="../assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

<link rel="stylesheet" href="../assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">



</head>



<body>

  <!-- Sidenav -->

  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">

    <div class="scrollbar-inner">

      <!-- Brand -->

          <br>
          <center>
            <h2 style="color:#0689f3;">VIL-GROUP</h2>
          </center>
         <!-- <img src="../assets/img/brand/vil.jpeg" width="90%" alt="..."> -->

    

      <div class="navbar-inner">

        <!-- Collapse -->

        <div class="collapse navbar-collapse" id="sidenav-collapse-main">

          <!-- Nav items -->

          <ul class="navbar-nav">

            <li class="nav-item">

              <a class="nav-link active" href="dashboard.php">

                <i class="ni ni-tv-2 text-primary"></i>

                <span class="nav-link-text">Dashboard</span>

              </a>

            </li>

            <li class="nav-item">

              <a class="nav-link" href="job.php">

                <i class="ni ni-collection text-primary"></i>

                <span class="nav-link-text">Data Lowongan</span>

              </a>

            </li>

            

            <li class="nav-item">

              <a class="nav-link" href="apliation.php">

                <i class="ni ni-collection text-primary"></i>

                <span class="nav-link-text">Data Kandidat</span>

              </a>

            </li>



            <li class="nav-item">

              <a class="nav-link dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" >

                <i class="ni ni-collection text-primary"></i>

                <span class="nav-link-text">List Kandidat </span>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                  <a class="dropdown-item" href="terpilih.php">Kandidat terpilih</a>

                  <a class="dropdown-item" href="tdk_terpilih.php">Kandidat Reject</a>

                  

                </div>

              </a>

            </li>


            <li class="nav-item">

              <a class="nav-link" href="psikotes.php">

                <i class="ni ni-collection text-primary"></i>

                <span class="nav-link-text">Proses Psikotes</span>

              </a>

            </li>
            
<!-- 
            <li class="nav-item">

              <a class="nav-link" href="interview.php">

                <i class="ni ni-collection text-primary"></i>

                <span class="nav-link-text">Proses Interview</span>

              </a>

            </li>

 -->

            <li class="nav-item">

              <a class="nav-link dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" >

                <i class="ni ni-collection text-primary"></i>

                <span class="nav-link-text">List Wawancara </span>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                  <a class="dropdown-item" href="interview.php">Proses Wawancara </a>

                  <a class="dropdown-item" href="wawancara_lolos.php">Lolos wawancara </a>

                   <a class="dropdown-item" href="wawancara_gagal.php">Gagal wawancara </a>

                </div>

              </a>

            </li>



            



          

          </ul>

          <!-- Divider -->

         

        </div>

      </div>

    </div>

  </nav>